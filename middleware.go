package main

import (
	"context"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"log"
	"net/http"
)

func ValidateTokenMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	token, err := request.ParseFromRequest(r, request.AuthorizationHeaderExtractor,
		func(token *jwt.Token) (interface{}, error) {
			return signKey, nil
		})

	if err == nil {
		if token.Valid {
			log.Println("Valid!")
			next(w, contextWithClaims(token, r))
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			fmt.Fprint(w, "Token is not valid")
		}
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(w, "Unauthorized access to this resource")
	}
}

func CurrentUserOrRoleMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

}

func contextWithClaims(t *jwt.Token, r *http.Request) *http.Request {
	claims := t.Claims.(jwt.MapClaims)
	ctx := context.WithValue(r.Context(), "user", claims["sub"])

	return r.WithContext(ctx)
}
