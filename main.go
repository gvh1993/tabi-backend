package main

import (
	//_ "github.com/jinzhu/gorm/dialects/sqlite"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/router"
	"gitlab.com/tabi/tabi-backend/server"
	"runtime"
)

var signKey = []byte("secret")

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func init() {

	// Use all CPU cores
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	log.Info("Starting API Server")

	s, httpserver := server.NewServer()
	defer s.Close()

	r := router.Load(s)
	log.WithFields(log.Fields{"host": s.Conf.Host}).Info("Listening")
	httpserver.Handler = r
	if httpserver.TLSConfig != nil {
		log.Fatal(httpserver.ListenAndServeTLS("", ""))
	} else {
		log.Fatal(httpserver.ListenAndServe())

	}
}
