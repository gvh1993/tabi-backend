package server

import (
	"crypto/tls"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
	"github.com/rifflock/lfshook"
	"gitlab.com/tabi/tabi-backend/config"
	"gitlab.com/tabi/tabi-backend/model"
	"golang.org/x/crypto/acme/autocert"
	"net/http"
	"os"
)

type GormLogger struct{}

func (*GormLogger) Print(v ...interface{}) {
	if v[0] == "sql" {
		log.WithFields(log.Fields{"module": "gorm", "type": "sql"}).Debug(v[3])
	}
	if v[0] == "log" {
		log.WithFields(log.Fields{"module": "gorm", "type": "log"}).Debug(v[2])
	}
}

type Server struct {
	DB   *gorm.DB
	Conf *config.Config
}

func getServer(host string, config config.LetsEncryptConfig) *http.Server {
	if !config.Enabled || !config.AcceptTOS {
		return &http.Server{
			Addr: host}
	}

	m := autocert.Manager{
		Cache:      autocert.DirCache("certs"),
		Prompt:     autocert.AcceptTOS,
		HostPolicy: autocert.HostWhitelist(config.Domain),
	}
	server := &http.Server{
		Addr:      ":https",
		TLSConfig: &tls.Config{GetCertificate: m.GetCertificate},
	}

	log.Info("Enabling LetsEncrypt")

	return server
}

func NewServer() (*Server, *http.Server) {
	conf, err := config.LoadTomlFile("server.toml")
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Fatal("Could not load configuration")
	}

	args := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s",
		conf.Postgres.Host, conf.Postgres.User, conf.Postgres.Database, conf.Postgres.Password)

	var db *gorm.DB

	if conf.DbType == config.Postgres {
		db, err = gorm.Open("postgres", args)
	} else if conf.DbType == config.Sqlite {
		db, err = gorm.Open("sqlite3", conf.Sqlite.Source)
	}

	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Fatal("Could not establish database connection")
	}

	log.SetOutput(os.Stdout)

	log.AddHook(lfshook.NewHook(lfshook.PathMap{
		log.InfoLevel : "info.log",
		log.ErrorLevel : "error.log",
	}, &log.JSONFormatter{}))

	if conf.LogLevel == config.Debug {
		db.LogMode(true)
		db.SetLogger(&GormLogger{})
	}

	lvl, err := log.ParseLevel(string(conf.LogLevel))
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Fatal("Could not parse loglevel string")
	}
	log.SetLevel(lvl)

	db.AutoMigrate(&model.User{}, &model.Device{}, &model.PositionEntry{}, &model.Attribute{}, &model.Log{}, &model.BatteryInfo{}, &model.UserStop{}, &model.UserStopVisit{})

	return &Server{
		db, &conf,
	}, getServer(conf.Host, conf.LetsEncrypt)
}

func (s *Server) Close() {
	s.DB.Close()
}
