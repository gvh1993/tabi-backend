package server

import (
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/auth"
	"gitlab.com/tabi/tabi-backend/model"
	"net/http"
	"strconv"
	"time"
)

func Hello(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprint(w, "hello, \n")
}

type UserMessage struct {
	Username  string
	FirstName string
	LastName  string
	Password  string
	Email     string
}

func Info(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		fmt.Fprint(w, "tabi-backend api")
	}
}

// PostRegister is the handler for the POST request that creates a new user.
func PostRegister(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		u := new(UserMessage)
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			return
		}
		err := json.NewDecoder(r.Body).Decode(u)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		user := model.User{Username: u.Username, Email: u.Email, FirstName: u.FirstName, LastName: u.LastName}
		user.SetPassword(u.Password)

		s.DB.Create(&user)

	}
}

func PostNewToken(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var u UserMessage
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			return
		}
		err := json.NewDecoder(r.Body).Decode(&u)
		if err != nil {
			http.Error(w, "Incorrectly formatted body", 400)
			return
		}

		var user model.User

		s.DB.Where("username = ?", u.Username).First(&user)

		start := time.Now()

		valid := user.VerifyPassword(u.Password)

		elapsed := time.Since(start)
		log.Printf("Bcrypt took %s", elapsed)

		if !valid {
			http.Error(w, "Incorrect information", http.StatusBadRequest)
			return
		}

		token, _ := auth.NewToken(user, []byte(s.Conf.SecretKey))
		log.WithFields(log.Fields{"id": user.ID, "username": user.Username}).Info("Created token for user")

		json.NewEncoder(w).Encode(token)

	}
}

func PostDevice(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}

		claims, err := auth.TokenFromRequest([]byte(s.Conf.SecretKey), r)
		if err != nil {
			log.WithField("error", err).Error("Could not get token from request")
			http.Error(w, "Token invalid", 400)
			return
		}

		var user model.User
		s.DB.Find(&user, claims.Subject)

		var device model.Device
		err = json.NewDecoder(r.Body).Decode(&device)
		if err != nil {
			log.WithField("error", err).Error("Could not decode body")
			http.Error(w, "Could not decode body", 400)
			return
		}

		if err = s.DB.Create(&device).Error; err != nil {
			log.WithField("error", err).Error("Could not create device")
			http.Error(w, "Could not store object", 500)
			return
		}

		user.Devices = append(user.Devices, device)
		if err = s.DB.Save(&user).Error; err != nil {
			log.WithField("error", err).Error("Could not save user")
			http.Error(w, "Could not store object", 500)
			return
		}

		json.NewEncoder(w).Encode(device)
	}
}

func GetDevices(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var user model.User
		s.DB.Find(&user, ps.ByName("uid"))
		var devices []model.Device
		s.DB.Model(&user).Preload("Attributes").Related(&devices)
		json.NewEncoder(w).Encode(devices)
	}
}

func GetDevice(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		uidStr := ps.ByName("uid")
		did := ps.ByName("did")
		var device model.Device
		s.DB.Preload("Attributes").Where("unique_identifier = ?", did).First(&device)
		uid, err := strconv.Atoi(uidStr)
		if err != nil {
			http.Error(w, "Incorrect userid", 400)
			return
		}

		if device.UserID != uint(uid) {
			http.Error(w, "Not found", 400)
			return
		}

		json.NewEncoder(w).Encode(device)
	}
}

func GetPositionEntries(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var device model.Device

		did := ps.ByName("did")
		s.DB.Where("unique_identifier = ?", did).First(&device)

		var positionentries []model.PositionEntry
		s.DB.Model(&device).Related(&positionentries)
		json.NewEncoder(w).Encode(positionentries)
	}
}

type attributeMessage struct {
	Value string
}

func GetAttribute(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		attrKey := ps.ByName("attr")

		did := ps.ByName("did")
		userId := ps.ByName("uid")

		var device model.Device
		s.DB.Where("unique_identifier = ?", did).First(&device)

		userIdInt, _ := strconv.Atoi(userId)
		if uint(userIdInt) != device.UserID {
			http.Error(w, "Unauthorized", 400)
			return
		}

		var attribute model.Attribute

		if err := s.DB.Where("key = ? and device_id = ?", attrKey, device.ID).Find(&attribute).Error; err != nil {
			http.NotFound(w, r)
			return
		}

		json.NewEncoder(w).Encode(attribute)
	}
}

func DeleteAttribute(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		attrKey := ps.ByName("attr")

		did := ps.ByName("did")
		userId := ps.ByName("uid")

		var device model.Device
		s.DB.Find(&device, did)

		userIdInt, _ := strconv.Atoi(userId)
		if uint(userIdInt) != device.UserID {
			http.Error(w, "Unauthorized", 400)
			return
		}

		var attribute model.Attribute

		s.DB.Where("key = ? and device_id = ?", attrKey, did).Find(&attribute)
		s.DB.Delete(&attribute)
	}
}

func PutAttribute(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		attrKey := ps.ByName("attr")
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			return
		}

		did := ps.ByName("did")
		userId := ps.ByName("uid")

		var message attributeMessage

		err := json.NewDecoder(r.Body).Decode(&message)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		var device model.Device
		s.DB.Where("unique_identifier = ?", did).First(&device)

		userIdInt, err := strconv.Atoi(userId)
		if uint(userIdInt) != device.UserID {
			http.Error(w, "Unauthorized", 400)
			return
		}

		attribute := model.Attribute{DeviceID: device.ID, Key: attrKey}
		s.DB.Unscoped().FirstOrCreate(&attribute)
		attribute.DeletedAt = nil
		attribute.Value = message.Value
		s.DB.Unscoped().Save(&attribute)
	}
}

func PostPositionEntries(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did := ps.ByName("did")

		var device model.Device
		s.DB.Where("unique_identifier = ?", did).First(&device)

		var pos []model.PositionEntry
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			return
		}
		err := json.NewDecoder(r.Body).Decode(&pos)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		device.PositionEntries = append(device.PositionEntries, pos...)
		s.DB.Save(&device)
	}

}

func PostStopVisits(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did := ps.ByName("did")

		var device model.Device
		s.DB.Where("unique_identifier = ?", did).First(&device)

		var stopVisits []model.UserStopVisit
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			return
		}
		err := json.NewDecoder(r.Body).Decode(&stopVisits)
		if err != nil {
			log.WithFields(log.Fields{"error": err}).Debug("Could not decode to stopsvisits")
			http.Error(w, err.Error(), 400)
			return
		}
		for _, value := range stopVisits {
			value.DeviceID = device.ID
		}

		log.WithFields(log.Fields{"stopVisits": stopVisits, "device": device}).Debug("Post Stop Visits")

		device.StopVisits = append(device.StopVisits, stopVisits...)
		s.DB.Save(&device)
	}

}

func PostLogs(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		uid := ps.ByName("uid")

		uidInt, err := strconv.Atoi(uid)
		if err != nil {
			http.Error(w, err.Error(), 400)
		}

		did := ps.ByName("did")

		var device model.Device
		s.DB.Where("unique_identifier = ?", did).First(&device)

		var logs []model.Log
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}
		err = json.NewDecoder(r.Body).Decode(&logs)
		if err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json logs")
			return
		}

		// GORM has no batch insert...
		for _, value := range logs {
			value.DeviceId = device.ID
			value.UserId = uint(uidInt)
			if err = s.DB.Create(&value).Error; err != nil {
				log.WithField("error", err).Error("could not save logs")
				http.Error(w, "could not save logs", 400)
			}
		}
	}
}

func PostBatteryInfo(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did := ps.ByName("did")

		var device model.Device
		s.DB.Where("unique_identifier = ?", did).First(&device)

		var batteryInfos []model.BatteryInfo
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}
		err := json.NewDecoder(r.Body).Decode(&batteryInfos)
		if err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json batteryinfo")
			return
		}

		for _, value := range batteryInfos {
			value.DeviceId = device.ID
			if err = s.DB.Create(&value).Error; err != nil {
				log.WithField("error", err).Error("could not save batteryinfo")
				http.Error(w, "could not save batteryinfo", 400)
			}
		}

	}
}

func GetLogs(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did := ps.ByName("did")

		var logs []model.Log
		var device model.Device
		s.DB.Where("unique_identifier = ?", did).First(&device)

		s.DB.Where("device_id = ?", device.ID).Find(&logs)

		json.NewEncoder(w).Encode(logs)

	}
}

func GetBatteryInfo(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did := ps.ByName("did")

		var batteryInfos []model.BatteryInfo
		var device model.Device
		s.DB.Where("unique_identifier = ?", did).First(&device)

		s.DB.Where("device_id = ?", device.ID).Find(&batteryInfos)

		json.NewEncoder(w).Encode(batteryInfos)

	}
}

func GetIsDeviceUnauthorized(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did := r.URL.Query().Get("device")

		var device model.Device
		s.DB.Where("unique_identifier = ?", did).First(&device)
		log.WithField("device", device).Info("Device authorization requested")

		var user model.User
		s.DB.First(&user, device.UserID)
		log.WithField("user", user).Info("Device authorization requested")
		if user.Disabled {
			w.WriteHeader(http.StatusUnauthorized)
		} else {
			w.WriteHeader(http.StatusOK)
		}
	}
}

func PostStops(s *Server) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		did := ps.ByName("did")

		var device model.Device
		s.DB.Where("unique_identifier = ?", did).First(&device)

		var batteryInfos []model.BatteryInfo
		if r.Body == nil {
			http.Error(w, "Empty request body", 400)
			log.Error("Empty request body")
			return
		}
		err := json.NewDecoder(r.Body).Decode(&batteryInfos)
		if err != nil {
			http.Error(w, "could not decode json", 400)
			log.WithField("error", err).Error("could not decode json batteryinfo")
			return
		}

		for _, value := range batteryInfos {
			value.DeviceId = device.ID
			if err = s.DB.Create(&value).Error; err != nil {
				log.WithField("error", err).Error("could not save batteryinfo")
				http.Error(w, "could not save batteryinfo", 400)
			}
		}

	}
}
