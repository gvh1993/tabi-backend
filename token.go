package main

import (
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"time"
)

func NewToken() {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"foo": "bar",
		"nbf": time.Date(2015, 10, 10, 12, 0, 0, 0, time.UTC).Unix(),
	})
	fmt.Println(token)
}

type UserAuthentication struct {
	Username string
	Password string
}
