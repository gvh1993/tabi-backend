package router

import (
	"github.com/julienschmidt/httprouter"
	"gitlab.com/tabi/tabi-backend/model"
	"gitlab.com/tabi/tabi-backend/router/middleware"
	"gitlab.com/tabi/tabi-backend/server"
	"net/http"
)

const (
	apiV1 string = "/api/v1"
)

func Load(s *server.Server) http.Handler {
	router := httprouter.New()

	authorizationMiddleware := middleware.UserResourceAuthorizationMiddleware{Signkey: []byte(s.Conf.SecretKey), UserIdentifier: "uid", AllowedRoles: []model.Role{model.Admin}}
	// Middleware to check if device belongs to user!

	roleMiddleware := middleware.RoleAuthorizationMiddleware{Signkey: []byte(s.Conf.SecretKey), DefaultAllow: false, AllowedRoles: []model.Role{model.Admin}}

	mid := middleware.New()
	mid.UseHandler(&authorizationMiddleware)

	midAdmin := middleware.New()
	midAdmin.UseHandler(&roleMiddleware)

	fs := http.FileServer(http.Dir("public_html"))
	router.Handler("GET", "/", fs)
	router.GET("/api/v1", server.Info(s))
	router.POST("/api/v1/register", server.PostRegister(s))
	router.POST("/api/v1/token", server.PostNewToken(s))
	router.GET("/api/v1/validate_device", server.GetIsDeviceUnauthorized(s))

	router.GET("/api/v1/user/:uid/device", mid.UseHandleAndServe(server.GetDevices(s)))
	router.POST("/api/v1/user/:uid/device", mid.UseHandleAndServe(server.PostDevice(s)))
	router.GET("/api/v1/user/:uid/device/:did", mid.UseHandleAndServe(server.GetDevice(s)))

	router.GET("/api/v1/user/:uid/device/:did/attribute/:attr", mid.UseHandleAndServe(server.GetAttribute(s)))
	router.PUT("/api/v1/user/:uid/device/:did/attribute/:attr", mid.UseHandleAndServe(server.PutAttribute(s)))
	router.DELETE("/api/v1/user/:uid/device/:did/attribute/:attr", mid.UseHandleAndServe(server.DeleteAttribute(s)))

	router.GET("/api/v1/user/:uid/device/:did/positionentry", midAdmin.UseHandleAndServe(server.GetPositionEntries(s)))
	router.POST("/api/v1/user/:uid/device/:did/positionentry", mid.UseHandleAndServe(server.PostPositionEntries(s)))

	router.POST("/api/v1/user/:uid/device/:did/stopvisits", mid.UseHandleAndServe(server.PostStopVisits(s)))

	router.GET("/api/v1/user/:uid/device/:did/logs", midAdmin.UseHandleAndServe(server.GetLogs(s)))
	router.POST("/api/v1/user/:uid/device/:did/logs", mid.UseHandleAndServe(server.PostLogs(s)))

	router.GET("/api/v1/user/:uid/device/:did/battery", midAdmin.UseHandleAndServe(server.GetBatteryInfo(s)))
	router.POST("/api/v1/user/:uid/device/:did/battery", mid.UseHandleAndServe(server.PostBatteryInfo(s)))

	return router
}