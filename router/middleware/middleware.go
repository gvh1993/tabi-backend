package middleware

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type Manager struct {
	handlers   []Handler
	middleware internalMiddleware
}

type Handler interface {
	ServeHandle(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle)
}

type HandlerFunc func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle)

func (f HandlerFunc) ServeHandle(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {
	f(rw, r, ps, next)
}

func Wrap(handler httprouter.Handle) Handler {
	return HandlerFunc(func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {
		handler(rw, r, ps)
	})
}

func New(handlers ...Handler) *Manager {
	return &Manager{
		handlers:   handlers,
		middleware: build(handlers),
	}
}

func (m *Manager) With(handlers ...Handler) *Manager {
	return New(append(m.handlers, handlers...)...)
}

func (m *Manager) UseHandler(handler Handler) *Manager {
	if handler == nil {
		panic("handler cannot be nill")
	}

	m.handlers = append(m.handlers, handler)
	m.middleware = build(m.handlers)
	return m
}

func (m *Manager) UseHandlerFunc(handler func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle)) {
	m.UseHandler(HandlerFunc(handler))
}

func (m *Manager) GetHandle() httprouter.Handle {
	return func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		m.middleware.ServeHandle(rw, r, ps)
	}
}

func (m *Manager) UseHandleAndServe(handler httprouter.Handle) httprouter.Handle {
	manager := *m
	manager.UseHandler(Wrap(handler))
	return manager.GetHandle()
}

type internalMiddleware struct {
	handler Handler
	next    *internalMiddleware
}

func (im *internalMiddleware) ServeHandle(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	im.handler.ServeHandle(rw, r, ps, im.next.ServeHandle)
}

func build(handlers []Handler) internalMiddleware {
	var next internalMiddleware

	if len(handlers) == 0 {
		return voidMiddleware()
	} else if len(handlers) > 1 {
		next = build(handlers[1:])
	} else {
		next = voidMiddleware()
	}
	return internalMiddleware{handlers[0], &next}
}

func voidMiddleware() internalMiddleware {
	return internalMiddleware{
		HandlerFunc(func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {}),
		&internalMiddleware{},
	}
}
