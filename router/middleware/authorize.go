package middleware

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"gitlab.com/tabi/tabi-backend/auth"
	"gitlab.com/tabi/tabi-backend/model"
	"net/http"
)

type UserResourceAuthorizationMiddleware struct {
	// UserIdentifier is the string that represents the user in the URL and that will be matched against the token.
	UserIdentifier string

	// AllowedRoles contain the roles that are always allowed to access the resources
	AllowedRoles []model.Role

	Signkey []byte
}

func (u *UserResourceAuthorizationMiddleware) ServeHandle(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {
	token, err := request.ParseFromRequestWithClaims(r, request.AuthorizationHeaderExtractor, &auth.Claims{},
		func(token *jwt.Token) (interface{}, error) {
			return u.Signkey, nil
		})

	if err != nil {
		rw.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(rw, "Unauthorized access to this resource")
		return
	}

	if claims, ok := token.Claims.(*auth.Claims); ok && token.Valid {
		userId := ps.ByName(u.UserIdentifier)

		if userId == claims.Subject {
			next(rw, r, ps)
		} else {
			rw.WriteHeader(http.StatusUnauthorized)
			fmt.Fprint(rw, "User is not authorized")
			return
		}
	} else {
		rw.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(rw, "Token is not valid")
		return
	}
}

func (u *UserResourceAuthorizationMiddleware) Handle(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		token, err := request.ParseFromRequestWithClaims(r, request.AuthorizationHeaderExtractor, &auth.Claims{},
			func(token *jwt.Token) (interface{}, error) {
				return u.Signkey, nil
			})

		if err != nil {
			unauthorized(w, nil, "unauthorized. could not parse token")
			return
		}

		if claims, ok := token.Claims.(*auth.Claims); ok && token.Valid {
			userId := ps.ByName(u.UserIdentifier)

			if userId == claims.Subject || matchRoles(u.AllowedRoles, claims.Roles) {
				h(w, r, ps)
			} else {
				unauthorized(w, claims, "user is not authorized")

				return
			}
		} else {
			unauthorized(w, claims, "token is not valid")
			return
		}

	}
}

type RoleAuthorizationMiddleware struct {
	AllowedRoles []model.Role
	DeniedRoles  []model.Role
	DefaultAllow bool
	Signkey      []byte
}

func (u *RoleAuthorizationMiddleware) ServeHandle(rw http.ResponseWriter, r *http.Request, ps httprouter.Params, next httprouter.Handle) {
	token, err := request.ParseFromRequestWithClaims(r, request.AuthorizationHeaderExtractor, &auth.Claims{},
		func(token *jwt.Token) (interface{}, error) {
			return u.Signkey, nil
		})

	if err != nil {
		unauthorized(rw, nil, "unauthorized. could not parse token")
		return
	}

	if claims, ok := token.Claims.(*auth.Claims); ok && token.Valid {
		if matchRoles(u.DeniedRoles, claims.Roles) {
			// User has a denied role. Deny access.
			unauthorized(rw, claims, "user has a role that has been denied access")
		} else if u.DefaultAllow || matchRoles(u.AllowedRoles, claims.Roles) {
			// User has an allowed role.
			next(rw, r, ps)
		} else {
			// Deny users
			unauthorized(rw, claims, "user is not authorized")
			return
		}
	} else {
		unauthorized(rw, claims, "token is not valid")
	}
}

func unauthorized(writer http.ResponseWriter, claims *auth.Claims, message string) {
	writer.WriteHeader(http.StatusUnauthorized)
	fmt.Fprint(writer, message)
	logrus.WithFields(logrus.Fields{"claims": claims, "message": message}).Info("User was denied access")
}

func matchRoles(findRoles []model.Role, claimRoles []model.Role) bool {
	for _, b := range claimRoles {
		if findRole(b, findRoles) {
			return true
		}
	}
	return false
}

func findRole(role model.Role, roles []model.Role) bool {
	for _, r := range roles {
		if r == role {
			return true
		}
	}
	return false
}
