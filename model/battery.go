package model

import (
	"github.com/jinzhu/gorm"
	"time"
)

type BatteryInfo struct {
	gorm.Model
	DeviceId     uint
	BatteryLevel int
	State        uint
	Timestamp    time.Time
}

type BatteryState uint

const (
	Discharging BatteryState = 0
	Charging                 = 1
	Full                     = 2
	NotCharging              = 3
	Unknown                  = 4
)
