package model

import (
	"golang.org/x/crypto/bcrypt"
	"time"
)

// Role constants represent the possible roles an user can be assigned.
type Role int

const (
	// Regular represents a regular user
	Regular Role = iota
	// Admin represents an administrator
	Admin
)

// User represents a user that can use the api.
type User struct {
	ID        uint
	Username  string `gorm:"not null;unique,unique_index"`
	FirstName string
	LastName  string
	Password  string
	Email     string
	Devices   []Device
	Logs      []Log
	Role      Role
	CreatedAt time.Time
	Deleted   bool
	Disabled  bool
}

// SetPassword hashes the given unhashed password, hashes it and stores it in the user.
func (u *User) SetPassword(password string) (err error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return
	}
	u.Password = string(bytes)

	return
}

// VerifyPassword accepts an unhashed password and verifies it. Returning true if the password is valid.
func (u User) VerifyPassword(password string) (valid bool) {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	return err == nil
}
