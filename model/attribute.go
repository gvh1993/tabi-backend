package model

import "time"

type Attribute struct {
	DeviceID  uint   `gorm:"primary_key"`
	Key       string `gorm:"primary_key"`
	Value     string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}
