package model

import (
	"time"
)

type PositionEntry struct {
	ID                              uint
	CreatedAt                       time.Time
	DeviceID                        uint
	Latitude                        float64
	Longitude                       float64
	Accuracy                        float32
	Speed                           float32
	Altitude                        float32
	DesiredAccuracy                 float32
	DistanceBetweenPreviousPosition float32
	Timestamp                       time.Time
}
type TokenResult struct {
	UserId uint
	Token  string
}

type RegisterUserMessage struct {
	Username string
	Email    string
	Password string
}
