package model

import (
	"time"
)

type Log struct {
	ID        uint
	Origin    string
	Event     string
	Message   string
	UserId    uint
	DeviceId  uint
	Timestamp time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}
