package model

import (
	"time"
)

type Device struct {
	ID               uint
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        *time.Time      `sql:"index"`
	UniqueIdentifier string          `gorm:"unique_index"`
	UserID           uint            `json:"-"`
	PositionEntries  []PositionEntry `json:",omitempty"`
	StopVisits       []UserStopVisit `json:",omitempty"`
	Attributes       []Attribute     `json:",omitempty"`
	Manufacturer     string          `json:",omitempty"`
	Model            string          `json:",omitempty"`
	OperatingSystem  string          `json:",omitempty"`
	Logs             []Log
}
