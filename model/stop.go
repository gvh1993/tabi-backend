package model

import (
	"github.com/jinzhu/gorm"
	"time"
)

type UserStop struct {
	gorm.Model
	Visits      []UserStopVisit
	PhoneStopId uint
	Name        string
	Latitude    float64
	Longitude   float64
}

type UserStopVisit struct {
	gorm.Model
	DeviceID         uint
	PhoneStopVisitId uint
	StopID           int
	Stop             UserStop
	BeginTimestamp   time.Time
	EndTimestamp     time.Time
}
