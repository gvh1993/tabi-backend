FROM golang:1.9-alpine AS builder

ENV GO_PROJECT_PATH gitlab.com/tabi/tabi-backend

WORKDIR /go/src/$GO_PROJECT_PATH

RUN apk --no-cache add git

COPY . .

RUN go build -v

FROM alpine:3.5

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates

WORKDIR /usr/local/bin

COPY --from=builder /go/src/gitlab.com/tabi/tabi-backend/tabi-backend .
COPY --from=builder /go/src/gitlab.com/tabi/tabi-backend/docker-server.toml server.toml

CMD ["./tabi-backend"]
